import React, {useState} from 'react';
import {connect} from 'react-redux';

import store from '../store';

const Add = (props) => {
  const [movieName, setMovieName] = useState('');

  const handleChange = (e) => {
    setMovieName(e.target.value)
  }

  const handleSubmit = () => {
    debugger
    props.addMovie(movieName)
    setMovieName('')
  }

  return (
    <div className="add">
      <h3>Add {props.type} Movie</h3>
     <input value={movieName} onChange={handleChange} placeholder="Enter Movie Name"/>
     <button onClick={handleSubmit} className="button">Add</button>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    type: state.type
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addMovie: (movieName) => {
      debugger
      dispatch({
        type: 'LIST:ADD',
        movieName
      })
    }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Add);
