import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import store from '../store';

const List = (props) => {

  return (
    <div className="list">
      <h3>Movies</h3>
     {props.moviesList.map((item) => 
       <div className="movie">{item}</div>
     )}
    </div>
  );
}

const mapStateToProps = (state) => {
  debugger
  return {
    moviesList: state.list
  };
}

const mapDispatchToProps = (dispatch) => {
 return {

 }
}

export default connect(mapStateToProps, mapDispatchToProps)(List);
